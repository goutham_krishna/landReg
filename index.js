
const express = require('express');
require('import-export');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const dbURI = "mongodb://localhost:27017/land";
const app = express();

const versions = require('./versioningRoutes/v1');
console.log("Versions");
console.log(versions);

mongoose.connect(dbURI);

mongoose.connection.on('connected',  ()=> {
    console.log('Mongoose default connection open to ' + dbURI);
});

mongoose.connection.on('error', (err)=> {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', ()=> {
    console.log('Mongoose default connection disconnected');
});

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json(true));

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
next();
});

app.use('/api',versions);

//************************************************ */
var myAppProperty = require('./app/javascripts/app-Property.js');
myAppProperty.start();
var myAppCreateUser = require('./app/javascripts/app-CreateUser.js');
myAppCreateUser.start();
//************************************************ */

const server = app.listen(8020, function () {
    let host = server.address().address;
    let port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);
});


