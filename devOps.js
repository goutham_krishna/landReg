const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const config = require('./config.js');

let bootSchema = new Schema({
        type: String
    },
    {strict: false}
);


let bootCollection = mongoose.model(config.collections.bootNodeCollection, bootSchema);

module.exports = bootCollection;