// Import libraries we need.
import Web3 from 'web3';

//Import/Set logger 
var loggerReq = require('./config/logger.js')
const accCollection = require('../../account.js');
var crypto = require('crypto');
var logger=loggerReq.loggerCreateUser;
//To config host & port
const config = require('../../config.js');

const keythereum = require("keythereum");

var web3;
var account;

var App = {
 
      start: function () {
                       var self = this;
                       web3 = new Web3(new Web3.providers.HttpProvider("http://"+config.development.host+":"+config.development.port));                       
                               //web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

                                    web3.eth.getAccounts(function (err, accs) {
                                            if (err != null) {
                                                logger.error("There was an error fetching your accounts.  "+err);
                                                //alert("There was an error fetching your accounts.");
                                                return;
                                            }
                                            if (accs.length == 0) {
                                                 logger.error("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.  "+err);
                                                 //alert("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
                                                 return;
                                             }
                                             // accounts = accs;
                                             // account = accounts[0];
                                             account = web3.eth.coinbase;
                                     });
       },
      createMember:function (req,res) {

                let password = req.password; 

                //Validation
                if(!password){
                  res({
                         "status_cd": 400,
                         "error" : {
                               "error_cd" : 400,
                               "message" : "Please enter a valid new password"
                         }
                  });
                 return;
                }

                let params = {keyBytes: 32, ivBytes: 16};
            
                let dk = keythereum.create(params);
            
                let kdf = "pbkdf2";
            
                let options = {
                        kdf: "pbkdf2",
                        cipher: "aes-128-ctr",
                        kdfparams: {
                            c: 262144,
                            dklen: 32,
                            prf: "hmac-sha256"
                        }
                };
            
                let keyObject = keythereum.dump(password, dk.privateKey, dk.salt, dk.iv, options);
                // console.log(keyObject);
                var keyObjectsString=JSON.stringify(keyObject);
                // console.log(keyObjectsString)
                var responseKeyObject= new Buffer(keyObjectsString).toString('base64'); 

                let publicAddress = "0x" + keyObject.address;
                //console.log(publicAddress);
                if(!publicAddress) {
                      res({
                              "status_cd": 400,
                              "error" : {
                                     "error_cd" : 400,
                                     "message" : "Error in creating address"
                              }
                      });
                      logger.error("createMember:ERROR");
               }  
               else {

                     res({
                            "status_cd": 200,
                            "data" : {
                                 "address":publicAddress,
                                 "key":responseKeyObject
                                                                           
                             }
                     });
                    logger.info("createMember:SUCCESS");
              }
            
      },
      createUser:function (req,res) {
      
              var email=req.email;
              var password=req.password;
              var type=req.type;
              var name=req.name;

              //Validation
              if(!password){
                          res({
                               "status_cd": 400,
                               "error" : {
                                        "error_cd" : 400,
                                        "message" : "Please enter a valid new password"
                                }
                          });
                          return;
              }
          if(!email){
              res({
                  "status_cd": 400,
                  "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid email"
                  }
              });
              return;
          }
          if(!type){
              res({
                  "status_cd": 400,
                  "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid user type"
                  }
              });
              return;
          }
          if(!name){
              res({
                  "status_cd": 400,
                  "error" : {
                      "error_cd" : 400,
                      "message" : "Please enter a valid name"
                  }
              });
              return;
          }
              var address =web3.personal.newAccount(password);
              if(!address) {
                            res({
                                   "status_cd": 400,
                                   "error" : {
                                        "error_cd" : 400,
                                        "message" : "Error in creating address"
                                   }
                            });
                            logger.error("createUser:ERROR");
              }  
              else {


                  var hash = crypto.createHash('sha256').update(password).digest('base64');
                  let AccCollection = new accCollection({
                      address: address,
                      private: hash,
                      email: email,
                      name: name,
                      type: type
                  });
                  AccCollection.save((err, data) => {
                      if (err) {
                          res({
                              "status_cd": 400,
                              "error" : {
                                  "error_cd" : 400,
                                  "message" : "Error in creating address"
                              }
                          });
                      }

                      logger.info("createUser:SUCCESS");
                      res({
                          "status_cd": 200,
                          "data" : {
                              "address":address
                          }
                      });

                  });

               }
      }
};
module.exports = App;