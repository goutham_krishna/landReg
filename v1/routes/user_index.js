
const express = require('express');
const accCollection = require('../../account.js');
const config = require('../../config.js');

const router = express.Router();

const myAppCreateUser = require('../../app/javascripts/app-CreateUser.js');
var crypto = require('crypto');
//To sign in
router.post('/signIn', function (req, res) {

    var email=req.body.email;
    var password=req.body.password;
    var type=req.body.type;

    //Validation
    if(!password){
        res.send({
            "status_cd": 400,
            "error" : {
                "error_cd" : 400,
                "message" : "Please enter a valid new password"
            }
        });
        return;
    }
    if(!email){
        res.send({
            "status_cd": 400,
            "error" : {
                "error_cd" : 400,
                "message" : "Please enter a valid email"
            }
        });
        return;
    }
    if(!type){
        res.send({
            "status_cd": 400,
            "error" : {
                "error_cd" : 400,
                "message" : "Please enter a valid user type"
            }
        });
        return;
    }

    var hash = crypto.createHash('sha256').update(password).digest('base64');

    accCollection.findOne({"email": email,"private":hash,"type":type}).lean().exec(function (err, out) {


        if (err) {
            return res.send({
                success: false,
                result: err
            });
        }
        if (!out) {
            return res.send({
                success: false,
                result: "Invalid address"
            });
        }
        else {

            res.send({
                success: true,
                result: out
            });
        }
    });


});

router.get('/getUsers', function (req, res) {

    accCollection.find({"type":"3"}).lean().exec(function (err, out) {


        if (err) {
            return res.send({
                success: false,
                result: err.toString()
            });
        }
        if (!out) {
            return res.send({
                success: false,
                result: "No users available"
            });
        }
        else {

            res.send({
                success: true,
                result: out
            });
        }
    });
});

module.exports = router;