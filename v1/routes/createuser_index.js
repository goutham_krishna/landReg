
const express = require('express');
const router = express.Router();

const myAppCreateUser = require('../../app/javascripts/app-CreateUser.js');

     //To create User
     router.post('/createUser', function (req, res) {

        // var password = req.body.password;
        myAppCreateUser.createUser(req.body,function(result){
            res.send(result);
        });

    });   
    //To test
    router.post('/test', function (req, res) {
               res.send({
                   success:"true"
               });
     }); 
module.exports = router;