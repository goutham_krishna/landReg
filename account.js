const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const config = require('./config.js');

let accountSchema = new Schema({
        address: String
    },
    {strict: false}
);


let accountCollection = mongoose.model(config.collections.accountCollection, accountSchema);

module.exports = accountCollection;